

public class MeritAmericaBankApp {

	
	
	public static void main(String[] args) {
		
		AccountHolder account = new AccountHolder();
		CheckingAccount checking = new CheckingAccount();
		SavingsAccount saving = new SavingsAccount();
		//account 1
		account.AccountHolder("John ", "James ", "Doe ", "123-45-6789 ", 100, 1000);
		checking.deposit(500);
		saving.withdraw(800);
		
		System.out.println(account.toString());
		
		System.out.println("-------------------- \n");
		
		//account 2
		account.AccountHolder("Doe ", "John ", "James ", "254-88-1546", 200, 500);
			
		
		checking.deposit(-500);
		saving.withdraw(600);
		
		System.out.println(account.toString());
		
		
		
	}
	

}
