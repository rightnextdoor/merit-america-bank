
public class SavingsAccount {
	
	public void SavingAccount(double openingBalance) {
		
		balance = openingBalance;
		//adding deposit
		balance += getDeposit(); 
		//check to see if you have enough money
		if (balance > getWithdraw()) {
			
			balance -= getWithdraw();
		}
	}
	
	public double getSavingAccount() {
		return balance;
	}
	
	public double getInterestRate() {
		rate = 0.01;
		return rate;
	}
	
	public boolean withdraw(double amount) {
		//check to see if the mount is not negative
		
		if (amount > 0 ) {
			
				withdrawBalance = amount;
			
			} else {
					withdrawBalance = 0;
					
				}
		
			
		return(amount > 0);
	}
	
	public double getWithdraw() {
		return withdrawBalance;
	}
		
		
	
	
	public boolean deposit(double amount) {
	
		//check to see if the mount is not negative
		if (amount > 0) {
			 
			depositBalance = amount;
			
			} else {
					depositBalance = 0;
				
				}
		
		return(amount > 0);
		
		}
	
	public double getDeposit() {
		
		return depositBalance;
	}
	
		
		public double futureValue(int years) {
		
			double a = 1 + getInterestRate();
			double b = years;
		
			future = (getSavingAccount() * Math.pow(a,b)) * 100;
			future = (int) future;
			future /= 100.00;
			return future;
		
	}
	
	public String toString() {
		return ("Saving Account Balance: " + getSavingAccount() + "\n"+
				"Saving Account Interest Rate: " + getInterestRate() +"\n"+
				"Saving Account Balance in 3 years: " + future);
	}
	
	private static double balance, rate, future,depositBalance,withdrawBalance;
	
}